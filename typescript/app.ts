import {Course} from "./course";
import {Voiture} from "./voiture";

let verte = new Voiture("Verte");
let rouge = new Voiture("Rouge");
let course = new Course("Lyon");

course.ajouteVoiture(verte);
course.ajouteVoiture(rouge);

console.log(course.toString());