"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Course = void 0;
class Course {
    ajouteVoiture(voiture) {
        this._voitures.push(voiture);
    }
    constructor(_name) {
        this._name = _name;
        this._voitures = [];
    }
    toString() {
        return this._name + '\n' +
            this._voitures.map(voiture => voiture.toString() + '\n');
    }
}
exports.Course = Course;
