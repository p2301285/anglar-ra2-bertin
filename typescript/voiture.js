"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Voiture = void 0;
class Voiture {
    constructor(_couleur) {
        this._couleur = _couleur;
    }
    toString() {
        return `Voiture ${this._couleur}`;
    }
}
exports.Voiture = Voiture;
