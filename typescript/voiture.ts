export class Voiture {
    constructor(private _couleur: String){}

    toString(){
        return `Voiture ${this._couleur}`;
    }
}