import {Voiture} from "./voiture";
export class Course {
    private _voitures: Array<Voiture>;

    ajouteVoiture(voiture: Voiture) {
        this._voitures.push(voiture);
    }

    constructor(private _name: string){
        this._voitures = []
    }

    toString() {
        return this._name + '\n' + 
        this._voitures.map(voiture => voiture.toString()+'\n');
    }
}