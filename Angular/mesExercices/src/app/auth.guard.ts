import { CanActivateFn } from '@angular/router';
import { inject } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';


export const authGuard: CanActivateFn = (route, state) => {
  () => {
    const router = inject(Router);
    const authService = inject(AuthService);
    if (!authService.isLogged()) {
      router.navigateByUrl('/');
      return false;
    }
  }
  return true;
}
