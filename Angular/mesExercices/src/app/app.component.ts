import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { GestionExercicesComponent } from './gestion-exercices/gestion-exercices.component';
import { DetailExerciceComponent } from "./detail-exercice/detail-exercice.component";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, GestionExercicesComponent, DetailExerciceComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'mesExercices';
}
