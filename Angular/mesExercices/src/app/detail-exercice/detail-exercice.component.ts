import {Component, EventEmitter, Input, Output} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { ExerciceModel } from '../models/exercice-model';
import { DatePipe } from '@angular/common';
import { TitleCasePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { ExerciceService } from '../exercice.service';
import { RouterLink } from '@angular/router';
import { Router } from '@angular/router';


@Component({
  selector: 'app-detail-exercice',
  standalone: true,
  imports: [ RouterLink, MatCardModule, MatButtonModule, MatCheckboxModule, DatePipe, TitleCasePipe],
  templateUrl: './detail-exercice.component.html',
  styleUrl: './detail-exercice.component.css'
})
export class DetailExerciceComponent {
  @Input({required : true}) selected:ExerciceModel | undefined
  
  @Output()
  estRendue = new EventEmitter<void>();

  id!:number

  constructor(private exerciceService: ExerciceService, private route: ActivatedRoute, private router: Router)
  {
    this.id = this.route.snapshot.params['id'];
    this.selected = this.exerciceService.getExercice(this.id);
  }
  
  changeRendu(){
    this.estRendue.emit();
  }

  onSuivant(){
    const id =this.exerciceService.getIdVoisin(this.id, 1);
    this.changeUrl(id);
  }

  onPrecedent(){
    const id =this.exerciceService.getIdVoisin(this.id, -1);
    this.changeUrl(id);
  }

  changeUrl(id:number){
    this.router.navigate(['/exercice/', id]);
  }
}
