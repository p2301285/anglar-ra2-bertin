import { Component } from '@angular/core';
import { ExerciceModel } from '../models/exercice-model';
import { ExerciceService } from '../exercice.service';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormField } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { JsonPipe } from '@angular/common';
ExerciceService
@Component({
  selector: 'app-create-exercice',
  standalone: true,
  imports: [ 
    ReactiveFormsModule,
    MatFormField,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    JsonPipe
  ],
  templateUrl: './create-exercice.component.html',
  styleUrl: './create-exercice.component.css'
})
export class CreateExerciceComponent {

}
