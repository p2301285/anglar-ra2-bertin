import { Injectable } from '@angular/core';
import { ExerciceModel } from './models/exercice-model';

@Injectable({
  providedIn: 'root'
})

export class ExerciceService {
  constructor() {}
  tabExercices : ExerciceModel[] = [
    {
      id: 1,
      nom: 'intro Typescript',
      dateDeRendu: new Date(2024, 9, 27),
      rendu: true
    },
    {
      id: 2,
      nom: 'démarrage du joli gestionnaire de devoirs',
      dateDeRendu: new Date(2024, 11, 3),
      rendu: false
    },
    {
      id: 3,
      nom: 'ajout d\'un routeur',
      dateDeRendu: new Date(2024, 12, 22),
      rendu: false
    }
  ]

  getExercice(id:number):ExerciceModel{
    return this.tabExercices.find((elt) => elt.id === id) as ExerciceModel;
  }

  createExercice(exo:ExerciceModel){
    this.tabExercices.push(exo);
  }

  deleteExerciceById(id: number): void {
    const index = this.tabExercices.findIndex((elt) => elt.id === id);
    this.tabExercices.splice(index, 1);
  }

  getIdVoisin(id: number, decalage: number): number {
    let index = this.tabExercices.findIndex((elt) => elt.id === id);
    index += decalage;
    if (index >= this.tabExercices.length)
      index = 0;
    else if (index < 0)
      index = this.tabExercices.length - 1;
    return (this.tabExercices[index].id);
  }

  updateRendu(id: number): void {
    const index = this.tabExercices.findIndex((elt) => elt.id === id);
    this.tabExercices[index].rendu = !this.tabExercices[index].rendu;
  }
}