import { Component } from '@angular/core';
import { ExerciceModel } from '../models/exercice-model';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import { DetailExerciceComponent } from "../detail-exercice/detail-exercice.component";
import { TitleCasePipe } from '@angular/common';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-gestion-exercices',
  standalone: true,
  imports: [RouterLink, MatListModule, MatButtonModule, MatCardModule, MatDividerModule, DetailExerciceComponent, TitleCasePipe],
  templateUrl: './gestion-exercices.component.html',
  styleUrl: './gestion-exercices.component.css'
})
export class GestionExercicesComponent {
  selected!:ExerciceModel
  tabExercices : ExerciceModel[] = [
    {
      id: 1,
      nom: 'intro Typescript',
      dateDeRendu: new Date(2024, 9, 27),
      rendu: true
    },
    {
      id: 2,
      nom: 'démarrage du joli gestionnaire de devoirs',
      dateDeRendu: new Date(2024, 11, 3),
      rendu: false
    },
    {
      id: 3,
      nom: 'ajout d\'un routeur',
      dateDeRendu: new Date(2024, 12, 22),
      rendu: false
    }
  ]

  aEteRendue(exo:ExerciceModel){
    this.selected = exo
  }

  onEstRendue(){
    const index = this.tabExercices.findIndex((elt) => elt.id ===this.selected?.id)
    this.tabExercices[index].rendu = !this.tabExercices[index].rendu;
  }
}
