import { Routes } from '@angular/router';
import { GestionExercicesComponent } from './gestion-exercices/gestion-exercices.component';
import { DetailExerciceComponent } from './detail-exercice/detail-exercice.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CreateExerciceComponent } from './create-exercice/create-exercice.component';
import { authGuard } from './auth.guard';
export const routes: Routes = [
    {
        path: '',
        component: GestionExercicesComponent,
        title: 'Gestion des exercices'
    },
    {
        path: 'exercice/:id',
        component: DetailExerciceComponent,
        title: 'Détail de l\'exercice'
    },
    {
        path: 'ajout',
        component: CreateExerciceComponent,
        title: 'Ajout',
        canActivate: [authGuard]
    },
    {
        path: '**',
        component: PageNotFoundComponent,
        title: 'Page non trouvée'
    }
];
