import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { AlerteComponent } from "./alerte/alerte.component";
import { MessageModel } from "./models/message-model";
import { MessageService } from './services/message.service';
import { Observable } from 'rxjs';
import { AsyncPipe } from '@angular/common';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, AlerteComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'monPremierProjet';
  messages!: Observable<MessageModel[]>;
  showMessages: boolean = true;

  constructor(private messageService: MessageService) {}

  ngOnInit() {
    this.messages = this.messageService.currentMessages;   
  }

  toggleMessages() {
    this.showMessages = !this.showMessages;
  }
}