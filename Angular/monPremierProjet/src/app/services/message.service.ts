import { Injectable } from '@angular/core';
import { MessageModel } from '../models/message-model';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class MessageService {

  private apiUrl = 'https://6710cf4ca85f4164ef2f67b0.mockapi.io/api/';
  private messagesSubject = new BehaviorSubject<MessageModel[]>([]);
  public currentMessages = this.messagesSubject.asObservable();

  constructor(private http: HttpClient){
    this.getAllMessages();
  }

  getAllMessages(): void{
    this.http.get<MessageModel[]>(`${this.apiUrl}/messages`)
      .subscribe({
        next: (messages) => this.messagesSubject.next(messages),
        error: (error) => console.log('http error: ', error)
      });

  }

  deleteMessage(id: number): void {
    this.http.delete<MessageModel>(`${this.apiUrl}/messages/${id}`)
      .subscribe({
        next: (removedMessage) => {
          this.messagesSubject
      .next(this.messagesSubject.value
        .filter(message => message.id != removedMessage.id));
          console.log('message supprimé:');
          console.log(removedMessage);
        },
        error: (error) => console.log('http error: ', error)
      }
    );
  }
}