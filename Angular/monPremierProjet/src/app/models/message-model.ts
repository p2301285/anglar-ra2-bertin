export interface MessageModel {
  id: number
  texte: string,
  nature: 'info' | 'validation' | 'warning'
  lu: boolean
  date: Date
}
