import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'inverser',
  standalone: true
})
export class InverserPipe implements PipeTransform {

  transform(message : string): unknown {
    let inverse = "";
    for (let index = 0; index < message.length; index++) {
      inverse += message[message.length - 1 -index];
      
    }
    return inverse
  }

}
