import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { MessageModel } from "../models/message-model";
import { MessageService } from '../services/message.service';
import { InverserPipe } from '../inverser.pipe';
import { DatePipe } from '@angular/common';
import { interval, Subscription } from 'rxjs';

@Component({
  selector: 'app-alerte',
  standalone: true,
  imports: [InverserPipe, DatePipe],
  templateUrl: './alerte.component.html',
  styleUrl: './alerte.component.css'
})
export class AlerteComponent implements OnInit, OnDestroy {
  
  @Input()
  message!: MessageModel;

  private intervalSubscription!: Subscription;

  constructor(private messageService: MessageService) {}

  ngOnInit() {
    this.intervalSubscription = interval(1000).subscribe(seconds => {
      console.log(`Alerte affichée depuis ${seconds} secondes`);
    });
  }

  ngOnDestroy() {
    if (this.intervalSubscription) {
      this.intervalSubscription.unsubscribe();
    }
  }

  onClickButton() {
    this.messageService.deleteMessage(this.message.id);
  }
}